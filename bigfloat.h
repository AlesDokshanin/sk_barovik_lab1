#ifndef BIGFLOAT_H
#define BIGFLOAT_H

#include <string>
#include <iostream>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <type_traits>

using boost::lexical_cast;
using namespace boost::multiprecision;

enum RoundingType {TRIM, MATH, BANKERS};

class BigFloat
{
    public:
        typedef cpp_dec_float_100 value_type;
        static int precision;
        static int output_precision;
        static int rounding_type;

        static void add_trailing_zeros(std::string&, int n);

        BigFloat();
        BigFloat(const BigFloat&);
        BigFloat(const std::string&);
        BigFloat(const value_type&);

//        template<typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value, T>::type>
        template<typename T>
        BigFloat(const T& value)
        {
            std::string s = lexical_cast<std::string>(value);
            this->value = value_type(s);
        }

        std::string str() const;
        std::string str_pretty(bool trim_zeros=true, bool separate_thousands=true) const;

        BigFloat operator+(const BigFloat&) const;
        BigFloat operator-(const BigFloat&) const;
        BigFloat operator*(const BigFloat&) const;
        BigFloat operator/(const BigFloat&) const;
        bool operator<(const BigFloat&) const;
        bool operator==(const BigFloat&) const;

        friend std::ostream& operator<< (std::ostream&, const BigFloat&);

        void output_round();

    protected:
        value_type value;

        void round(int n_decimals, int rounding_type);
        void calculation_round();

    private:
        static value_type drop_decimal_part(const value_type&);
        static void trim_zeros(std::string &str);
        static void separate_thousands(std::string &str);
};

#endif // BIGFLOAT_H
