#include <QWidget>
#include <QDebug>
#include <QString>
#include <QMessageBox>
#include <QRegExpValidator>
#include <QRegExp>
#include <string>

#include "bigfloatinput.h"
#include "bigfloat.h"


BigFloatInput::BigFloatInput(QWidget* parent)
{
    this->setValidator(new BigFloatValidator(this));
    QObject::connect(this, &BigFloatInput::editingFinished,
                     this, &BigFloatInput::prettify_value);
}

BigFloat BigFloatInput::value() const
{
    QString text = this->text();
    text.replace(' ', "");

    std::string str = text.toUtf8().constData();
    BigFloat f(str);
    return f;
}

void BigFloatInput::prettify_value()
{
    BigFloat value = this->value();
    std::string str = value.str_pretty(true, true);
    this->setText(QString::fromUtf8(str.c_str()));
}

BigFloatValidator::BigFloatValidator(QObject *parent)
{
    this->setRegExp(QRegExp(pattern));
}

void BigFloatValidator::fixup(QString &input) const
{
    if(input.endsWith('.'))
        input.remove(input.length() - 1, 1);
}

QValidator::State BigFloatValidator::validate(QString &input, int &pos) const
{
    input.replace(',', ".");
    State state = QRegExpValidator::validate(input, pos);
    return state;
}

QString BigFloatValidator::pattern = "^-?[\\d ]+(\\.\\d{1,3})?$";
