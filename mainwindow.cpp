#include <QDebug>
#include <QMessageBox>
#include <QComboBox>
#include <QChar>
#include <QString>
#include <QTableWidgetItem>
#include <algorithm>

#include "bigfloatinput.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    operator_inputs = QVector<QComboBox*>();
    operator_inputs.push_back(ui->cb_operator1);
    operator_inputs.push_back(ui->cb_operator2);
    operator_inputs.push_back(ui->cb_operator3);

    number_inputs = QVector<BigFloatInput*>();
    number_inputs.push_back(ui->le_num1);
    number_inputs.push_back(ui->le_num2);
    number_inputs.push_back(ui->le_num3);
    number_inputs.push_back(ui->le_num4);

    ui->de_start_date->setDate(QDate::currentDate());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btn_calculate_clicked()
{
    update_rounding_mode();

    QVector<QChar> operators;
    for(auto input: operator_inputs)
        operators.push_back(input->currentText().at(0));

    auto ops_without_last = QVector<QChar>(operators);
    ops_without_last.pop_back();

    auto order = this->get_order(ops_without_last);
    order.push_back(operators.length() - 1);

    QVector<BigFloat> numbers;
    try
    {
        for(auto input: number_inputs)
            numbers.push_back(input->value());

        auto result = calculate(numbers, operators, order);
        result.output_round();
        ui->label_result->setText(print_number_calc(result));
    }
    catch(std::runtime_error e)
    {
        QMessageBox::warning(this, "Ошибка", "Ошибка ввода или переполнение");
    }
}

void MainWindow::update_rounding_mode() const
{
    int rounding_type;
    if(ui->rb_rounding_bankers->isChecked())
        rounding_type = BANKERS;
    else if(ui->rb_rounding_math->isChecked())
        rounding_type = MATH;
    else if(ui->rb_rounding_trim->isChecked())
        rounding_type = TRIM;

    BigFloat::rounding_type = rounding_type;
}

QVector<int> MainWindow::get_order(const QVector<QChar> &operators)
{
    QVector<int> order;

    QVector<QChar> high_priority(QVector<QChar>() << '*' << '/');
    QVector<QChar> low_priority(QVector<QChar>() << '+' << '-');
    QVector<QChar> ops_priority[] = {high_priority, low_priority };

    for(auto ops: ops_priority)
    {
        for(int i = 0; i < operators.length(); i++)
        {
            auto op = operators[i];
            if(ops.indexOf(op) != -1)
                order.push_back(i);
        }
    }

    return order;
}

BigFloat MainWindow::calculate(const QVector<BigFloat> &numbers, const QVector<QChar> &operators, const QVector<int> &order)
{
    QVector<BigFloat*> num_ptrs;
    for(auto number: numbers)
        num_ptrs.push_back(new BigFloat(number));

    for(int index: order)
    {
        QChar op = operators[index];
        BigFloat* a = num_ptrs[index];

        int i = index + 1;
        BigFloat* b = num_ptrs[i];
        while(!b)
            b = num_ptrs[++i];

        BigFloat res = perform_operation(*a, *b, op);
        num_ptrs[i] = &res;
        num_ptrs[index] = 0;
    }

    return *num_ptrs[num_ptrs.length() - 1];
}

BigFloat MainWindow::perform_operation(const BigFloat &a, const BigFloat &b, const QChar &op)
{
    switch(op.toLatin1())
    {
        case '+':
            return a + b;

        case '-':
            return a - b;

        case '*':
            return a * b;

        case '/':
            return a / b;
    }
}

QString MainWindow::print_number_calc(const BigFloat &n)
{
    BigFloat num_copy(n);
    num_copy.output_round();
    std::string str = num_copy.str_pretty(true, true);
    return QString::fromUtf8(str.c_str());
}

QString MainWindow::print_number_credit_calc(const BigFloat &n)
{
    BigFloat num_copy(n);
    num_copy.output_round();
    std::string str = num_copy.str_pretty(false, true);
    BigFloat::add_trailing_zeros(str, 3);
    return QString::fromUtf8(str.c_str());
}

void MainWindow::on_btn_credit_calculate_clicked()
{
    try
    {
        BigFloat::rounding_type = MATH;
        QVector<QVector<QString> > data;
        QDate start = ui->de_start_date->date();

        BigFloat sum(ui->le_sum->value());
        BigFloat percent = BigFloat(1) + ui->le_percent->value() / BigFloat(100);
        BigFloat fixed_payment = ui->le_fixed_payment->value();
        BigFloat zero(0);

        QString date_format = "dd.MM.yyyy";
        for(int i = 0; i < 60; i++)
        {
            QDate end = start.addDays(29);
            QVector<QString> row;

            row.push_back(start.toString(date_format));

            row.push_back(end.toString(date_format));

            row.push_back(print_number_credit_calc(sum));

            sum = sum * percent;
            sum.output_round();
            row.push_back(print_number_credit_calc(sum));

            sum = sum - fixed_payment;
            sum.output_round();
            row.push_back(print_number_credit_calc(sum));

            data.push_back(row);
            start = end.addDays(1);

            if(sum < zero)
                break;
        }

        ui->tableWidget->setRowCount(data.length());
        for(int i = 0; i < data.length(); i++)
        {
            QVector<QString> row = data[i];
            for(int j = 0; j < row.length(); j++)
            {
                QTableWidgetItem* item = new QTableWidgetItem(row[j]);
                item->setTextAlignment(Qt::AlignRight);
                item->setFlags(Qt::ItemIsEnabled);
                ui->tableWidget->setItem(i, j, item);
            }
        }
    }
    catch(std::runtime_error e)
    {
        QMessageBox::warning(this, "Ошибка", "Ошибка ввода или переполнение");
    }
}
