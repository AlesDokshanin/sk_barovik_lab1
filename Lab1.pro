#-------------------------------------------------
#
# Project created by QtCreator 2016-03-15T11:27:16
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lab1
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    bigfloatinput.cpp \
    bigfloat.cpp

HEADERS  += mainwindow.h \
    bigfloat.h \
    bigfloatinput.h

FORMS    += mainwindow.ui

INCLUDEPATH += C:/boost_1_60_0

LIBS += -L"C:/boost_1_60_0/libs"

CONFIG += c++11
