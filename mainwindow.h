#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include <QChar>
#include <QComboBox>

#include "bigfloatinput.h"
#include "bigfloat.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btn_calculate_clicked();

    void on_btn_credit_calculate_clicked();

private:
    Ui::MainWindow *ui;
    QVector<QComboBox*> operator_inputs;
    QVector<BigFloatInput*> number_inputs;


    void update_rounding_mode() const;

    static QVector<int> get_order(const QVector<QChar>& ops);
    static BigFloat calculate(const QVector<BigFloat>& numbers, const QVector<QChar>& ops, const QVector<int>& order);
    static BigFloat perform_operation(const BigFloat& a, const BigFloat& b, const QChar& op);

    static QString print_number_calc(const BigFloat& n);
    static QString print_number_credit_calc(const BigFloat& n);


};

#endif // MAINWINDOW_H
