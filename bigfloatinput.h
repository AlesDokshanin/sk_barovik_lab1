#ifndef BIGFLOATINPUT_H
#define BIGFLOATINPUT_H

#include <QLineEdit>
#include <QObject>
#include <QValidator>
#include <QFocusEvent>
#include <QRegExpValidator>

#include "bigfloat.h"

class BigFloatInput : public QLineEdit
{
    Q_OBJECT

public:
    BigFloatInput(QWidget* parent = 0);
    BigFloat value() const;

private:
    void prettify_value();
};

class BigFloatValidator: public QRegExpValidator
{
    Q_OBJECT

public:
    BigFloatValidator(QObject* parent = 0);

    static QString pattern;

    virtual void fixup(QString &input) const;
    virtual State validate(QString &input, int &pos) const;
};


#endif // BIGFLOATINPUT_H
