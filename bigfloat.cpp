#include <cmath>
#include "boost/multiprecision/cpp_int.hpp"
#include <boost/lexical_cast.hpp>

#include "bigfloat.h"

int BigFloat::precision = 6;

int BigFloat::output_precision = 3;

int BigFloat::rounding_type = BANKERS;

std::ostream& operator<< (std::ostream& stream, const BigFloat& number)
{
    stream << number.value.str();
    return stream;
}

BigFloat::value_type BigFloat::drop_decimal_part(const value_type& val)
{
    BigFloat::value_type result;
    if(val < 0)
        result = boost::multiprecision::ceil(val);
    else
        result = boost::multiprecision::floor(val);

    return result;
}

void BigFloat::add_trailing_zeros(std::string &str, int n)
{
    int dot_pos = str.find('.');
    if(dot_pos == -1)
    {
        str += ".";
        for(int i = 0; i < n; i++)
            str += "0";
    }
    else
    {
        int decimal_part_length = str.length() - dot_pos - 1;
        int zeros_to_add = n - decimal_part_length;

        for(int i = 0; i < zeros_to_add; i++)
            str += "0";
    }
}

BigFloat::BigFloat(): value(0) {}

BigFloat::BigFloat(const BigFloat &a)
{
    this->value = value_type(a.value);
}

BigFloat::BigFloat(const BigFloat::value_type &v): value(v) {}

BigFloat::BigFloat(const std::string& s)
{
    this->value = BigFloat::value_type(s);
}

std::string BigFloat::str() const
{
    return this->value.str();
}

std::string BigFloat::str_pretty(bool trim_zeros, bool separate_thousands) const
{
    std::string str = this->value.str();
    if(trim_zeros)
        BigFloat::trim_zeros(str);

    if(separate_thousands)
        BigFloat::separate_thousands(str);

    return str;
}

void BigFloat::trim_zeros(std::string& str)
{
    int dot_index = str.find('.');
    bool has_decimal_part = dot_index != -1;

    if(has_decimal_part)
    {
        int n_zeros = 0;
        int i = str.length() - 1;
        while(str[i] == '0')
            n_zeros++;

        str = str.substr(0, str.length() - n_zeros);
    }
}

void BigFloat::separate_thousands(std::string& str)
{
    int dot_index = str.find('.');
    bool has_decimal_part = dot_index != -1;

    int end_of_integer_part = has_decimal_part ? dot_index - 1 : str.length() - 1;
    bool negative = str[0] == '-';
    for(int i = end_of_integer_part, c = 0; i >= 0 + (int)negative; i--, c++)
        if(c > 0 && c % 3 == 0)
            str.insert(i + 1, " ");
}

void BigFloat::round(int n_decimals, int rounding_type)
{
    BigFloat::value_type multiplied_copy = BigFloat::value_type(this->value);
    long multiplier = std::pow(10, n_decimals);
    multiplied_copy *= multiplier;

    switch(rounding_type)
    {
        case MATH:
            multiplied_copy = boost::multiprecision::round(multiplied_copy);
            break;

        case TRIM:
            multiplied_copy = this->drop_decimal_part(multiplied_copy);
            break;

        case BANKERS:
            {
                std::string tmp = this->drop_decimal_part(multiplied_copy * 10).str();
                int len = tmp.length();

                char last_digit = tmp[len -1];

                if(last_digit == '5')
                {
                    char one_before_last_digit = tmp[len - 2];
                    bool is_even = lexical_cast<int>(one_before_last_digit) % 2 == 0;
                    bool positive = this->value >= 0;

                    if(!(is_even ^ positive))
                        multiplied_copy = boost::multiprecision::floor(multiplied_copy);

                    else
                        multiplied_copy = boost::multiprecision::ceil(multiplied_copy);
                }

                else
                {
                    multiplied_copy = boost::multiprecision::round(multiplied_copy);
                    break;
                }
            }
            break;

        default:
            std::cerr << "Unexpected rounding type: " << this->rounding_type << std::endl;
            std::exit(1);
            break;
    }

    this->value = multiplied_copy / multiplier;
}

void BigFloat::calculation_round()
{
    return this->round(this->precision, MATH);
}

void BigFloat::output_round()
{
    return this->round(this->output_precision, this->rounding_type);
}

BigFloat BigFloat::operator+(const BigFloat &n) const
{
    BigFloat::value_type result_value(this->value + n.value);
    BigFloat result(result_value);
    result.calculation_round();
    return result;
}

BigFloat BigFloat::operator-(const BigFloat &n) const
{
    BigFloat::value_type result_value(this->value - n.value);
    BigFloat result(result_value);
    result.calculation_round();
    return result;
}

BigFloat BigFloat::operator*(const BigFloat &n) const
{
    BigFloat::value_type result_value(this->value * n.value);
    BigFloat result(result_value);
    result.calculation_round();
    return result;
}

BigFloat BigFloat::operator/(const BigFloat &n) const
{
    BigFloat::value_type result_value(this->value / n.value);
    BigFloat result(result_value);
    result.calculation_round();
    return result;
}

bool BigFloat::operator<(const BigFloat &a) const
{
    return this->value < a.value;
}

bool BigFloat::operator==(const BigFloat &a) const
{
    return this->value == a.value;
}
